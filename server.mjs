import serveHandler from "serve-handler";
import WebSocket, { WebSocketServer } from "ws";
import {createServer} from "http";
import fs from "fs";

var record = fs.createWriteStream("record.jsonl", {flags: "a"});

var server = createServer(serveHandler).listen(3000);
var wss = new WebSocketServer({server});

wss.on("connection", (ws, req) => {
	var id = dispenseId();
	ws.on("message", (data, isBinary) => {
		if (isBinary) return ws.close(1003);
		try {
			data = JSON.parse(data);
			data.i = id;
			data = JSON.stringify(data);
			record.write(data + '\n');
		} catch(error) {
			console.error(error.message);
			ws.close(1002);
		}
	});
	ws.on("error", error => console.error(error.message));
});



function dispenseId() {
	try {
		var id = Number(fs.readFileSync("nextid", "utf8"));
	} catch (e) {}
	if (!id || isNaN(id)) id = 0;
	fs.writeFileSync("nextid", String(id + 1));
	return id;
}